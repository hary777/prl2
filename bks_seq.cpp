
#include <mpi.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

using namespace std;
 
//#define MEASURE
//#define DEBUG

#define SOUBOR "numbers"	//nazev souboru




#define BUFSIZE 128
#define TAG 0

int main(int argc, char *argv[])
{
  #ifdef MEASURE
  struct timeval start, stop;
  #endif // MEASURE

  int numprocs;               //pocet procesoru
  int myid;                   //muj rank, od nuly
  MPI_Status mpi_mystat;      //struct- obsahuje kod- source, tag, error
  //MPI_Request mpi_myreq;

  //MPI INIT
  MPI_Init(&argc,&argv);                          // inicializace MPI
  MPI_Comm_size(MPI_COMM_WORLD, &numprocs);       // zjistíme, kolik procesů běží
  MPI_Comm_rank(MPI_COMM_WORLD, &myid);			  // zjistíme id svého procesu




  if(myid == 0)
  {
    ifstream soubor (SOUBOR, std::ifstream::binary);
    if(!soubor.good()){
      cerr << "chyba souboru";
    }

    vector<unsigned char> numbers;
    char c;
    while(soubor.get(c)){
      numbers.push_back(c);
    }

    for(auto i:numbers){
      cout << dec << +i << " ";
    }
    cout << endl;

    sort(numbers.begin(), numbers.end());

    for(auto i:numbers){
      cout << dec << +i << endl;
    }

  }
 

  MPI_Finalize(); 
  return 0;
}

