#!/bin/bash

#1 parametr a to pocet_hodnot, jinak se zvoli 8
if [ $# -lt 1 ];then 
    pocet_hodnot=8;
else
    pocet_hodnot=$1;
fi;


#vypocet poctu procesoru
pocet_listovych_cpu=$(echo "l($pocet_hodnot)/l(2)" | bc -l | cut -d'.' -f1);
pocet_listovych_cpu=$(echo "$pocet_listovych_cpu+1" | bc -l)
n=0
while [ $pocet_listovych_cpu -gt $(echo "2^$n" | bc -l) ]
do
n=$(echo "$n+1" | bc -l)
done
pocet_listovych_cpu=$(echo "2^$n" | bc -l)
pocet_cpu_celkem=$(echo "($pocet_listovych_cpu*2)-1" | bc -l)

pocet_procesoru=$pocet_cpu_celkem;

#preklad cpp zdrojaku
mpic++ --prefix /usr/local/share/OpenMPI -Wall -o bks bks.cpp
mpic++ --prefix /usr/local/share/OpenMPI -o bks_seq bks_seq.cpp

#vyrobeni souboru s random cisly
dd if=/dev/urandom bs=1 count=$pocet_hodnot of=numbers status=none

#spusteni
mpirun --allow-run-as-root --prefix /usr/local/share/OpenMPI -np $pocet_procesoru bks > bks.txt
mpirun --allow-run-as-root --prefix /usr/local/share/OpenMPI -np 1 bks_seq > bks_seq.txt

#porovnani
diff bks.txt bks_seq.txt
if [ $? -eq 0 ]; then
    echo "Test OK";
fi;

#uklid
rm -f bks bks_seq bks.txt bks_seq.txt numbers

