
#include <mpi.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <cmath>

#include <sys/stat.h>
#include <sys/time.h>

using namespace std;
 
//#define MEASURE
//#define DEBUG

#define SOUBOR "numbers"	//nazev souboru
#define MAX_VALUE 255

#define CHILD_LEFT(i)  ((2*i)+1)
#define CHILD_RIGHT(i) ((2*i)+2)
#define PARENT(i)      ((i-1)/2)
#define IAM_LIST(i, numprocs) ((i >= ((numprocs-1)/2) )? true : false)
#define IAM_ROOT(i) ((i == 0 )? true : false)
#define IAM_TREE(i, numprocs) (( !IAM_ROOT(i) && !IAM_LIST(i, numprocs) )? true : false)
#define GET_TREE_LEVEL(i) (floor(log2(i+1))) //root is level 0

#define TAG           0
#define TAG_VELIKOST	1
#define TAG_DATA			2

int main(int argc, char *argv[])
{
  #ifdef MEASURE
  struct timeval start, stop;
  #endif // MEASURE

  int numprocs;               //pocet procesoru
  int myid;                   //muj rank, od nuly
  MPI_Status mpi_mystat;      //struct- obsahuje kod- source, tag, error
  //MPI_Request mpi_myreq;

  //MPI INIT
  MPI_Init(&argc,&argv);                          // inicializace MPI
  MPI_Comm_size(MPI_COMM_WORLD, &numprocs);       // zjistíme, kolik procesů běží
  MPI_Comm_rank(MPI_COMM_WORLD, &myid);			      // zjistíme id svého procesu



  /*prvni procesor nacte soubor, vypise na vystup, rozesle data listovym procesorum*/
  if(IAM_ROOT(myid))
  {
    ifstream soubor (SOUBOR, std::ifstream::binary);
    if(!soubor.good()){
      cerr << "chyba souboru";
    }

    //nacteni
    vector<unsigned char> numbers;
    char c;
    while(soubor.get(c)){
      numbers.push_back(c);
    }

    //vypis
    for(auto i:numbers){
      cout << dec << +i << " ";
    }
    cout << endl;

    //vypocet poctu
    int pocet_prvku = numbers.size();
    int pocet_listovych_cpu = ceil(log2(pocet_prvku));

    /*aby byl zarovnany binarni strom 
    musi byt pocet listovych cpu zarovnany na 2^n*/
    int n = 0;
    while(pocet_listovych_cpu > pow(2, n)){
      n++;
    }
    pocet_listovych_cpu = pow(2, n);
    int pocet_prvku_na_cpu = ceil( (float)pocet_prvku / (float)pocet_listovych_cpu );
    int pocet_mist_na_listovych_cpu = pocet_prvku_na_cpu * pocet_listovych_cpu;
    int pocet_mist_navic = pocet_mist_na_listovych_cpu - pocet_prvku;

    #ifdef MEASURE
		gettimeofday(&start, NULL);
		#endif // MEASURE

    //odeslani velikosti listovym procesorum
    for(int i=((numprocs-1)/2); i < numprocs; i++){
      MPI_Send(&pocet_prvku_na_cpu, 1, MPI_INT, i, TAG_VELIKOST, MPI_COMM_WORLD);
    }

    //odeslani dat listovym procesorum
    unsigned char* buf = numbers.data();
    int posuv_indexu = 0;
    for(int i=((numprocs-1)/2); i < numprocs; i++){
      if(posuv_indexu < (pocet_prvku - pocet_prvku_na_cpu) ){
        MPI_Send(buf + posuv_indexu, pocet_prvku_na_cpu, MPI_CHAR, i, TAG_DATA, MPI_COMM_WORLD);
        posuv_indexu += pocet_prvku_na_cpu;
      }
      else{
        unsigned char buf2[pocet_prvku_na_cpu];
        for(int j=0; j < pocet_prvku_na_cpu; j++){
          if(posuv_indexu < pocet_prvku){
            buf2[j] = numbers[posuv_indexu];
            posuv_indexu++;
          }
          else{
            buf2[j] = MAX_VALUE;
          }
        }
        MPI_Send(buf2, pocet_prvku_na_cpu, MPI_CHAR, i, TAG_DATA, MPI_COMM_WORLD);
      }
    }

    //data odeslana, bezi vypocet

    //posledni spojeni posloupnosti a vypis
    int pocet_prvku_levy, pocet_prvku_pravy;
    MPI_Recv(&pocet_prvku_levy, 1, MPI_INT, CHILD_LEFT(myid), TAG_VELIKOST, MPI_COMM_WORLD, &mpi_mystat);
    MPI_Recv(&pocet_prvku_pravy, 1, MPI_INT, CHILD_RIGHT(myid), TAG_VELIKOST, MPI_COMM_WORLD, &mpi_mystat);
    int pocet_prvku_celkem = pocet_prvku_levy + pocet_prvku_pravy;

    //prijem dat
    vector<unsigned char> buf_levy(pocet_prvku_levy);
    vector<unsigned char> buf_pravy(pocet_prvku_pravy);
    MPI_Recv(buf_levy.data(), pocet_prvku_levy, MPI_CHAR, CHILD_LEFT(myid), TAG_DATA, MPI_COMM_WORLD, &mpi_mystat);
    MPI_Recv(buf_pravy.data(), pocet_prvku_pravy, MPI_CHAR, CHILD_RIGHT(myid), TAG_DATA, MPI_COMM_WORLD, &mpi_mystat);

    //merge
    vector<unsigned char> buf_vystup(pocet_prvku_celkem);
    merge(buf_levy.begin(), buf_levy.end(), buf_pravy.begin(), buf_pravy.end(), buf_vystup.begin());

    #ifdef MEASURE
		gettimeofday(&stop, NULL);
		int usec = stop.tv_usec - start.tv_usec;
		cerr << usec << endl;
		#endif // MEASURE

    //vypis
    for(uint i=0; i < buf_vystup.size()-pocet_mist_navic; i++){
      cout << dec << +buf_vystup[i] << endl;
    }

    //konec
  }
 

  //listove procesory, seradi data a posle rodicum
  if(IAM_LIST(myid, numprocs))
  {
    int pocet_prvku_na_cpu;
	  MPI_Recv(&pocet_prvku_na_cpu, 1, MPI_INT, 0, TAG_VELIKOST, MPI_COMM_WORLD, &mpi_mystat);

    //prijem dat
    vector<unsigned char> buf(pocet_prvku_na_cpu);
    MPI_Recv(buf.data(), pocet_prvku_na_cpu, MPI_CHAR, 0, TAG_DATA, MPI_COMM_WORLD, &mpi_mystat);

    //razeni
    sort(buf.begin(), buf.end());

    //odeslani rodici
    MPI_Send(&pocet_prvku_na_cpu, 1, MPI_INT, PARENT(myid), TAG_VELIKOST, MPI_COMM_WORLD);
    MPI_Send(buf.data(), pocet_prvku_na_cpu, MPI_CHAR, PARENT(myid), TAG_DATA, MPI_COMM_WORLD);
  }


  if(IAM_TREE(myid,numprocs))
  {
    int pocet_prvku_levy, pocet_prvku_pravy;
    MPI_Recv(&pocet_prvku_levy, 1, MPI_INT, CHILD_LEFT(myid), TAG_VELIKOST, MPI_COMM_WORLD, &mpi_mystat);
    MPI_Recv(&pocet_prvku_pravy, 1, MPI_INT, CHILD_RIGHT(myid), TAG_VELIKOST, MPI_COMM_WORLD, &mpi_mystat);
    int pocet_prvku_celkem = pocet_prvku_levy + pocet_prvku_pravy;

    //prijem dat
    vector<unsigned char> buf_levy(pocet_prvku_levy);
    vector<unsigned char> buf_pravy(pocet_prvku_pravy);
    MPI_Recv(buf_levy.data(), pocet_prvku_levy, MPI_CHAR, CHILD_LEFT(myid), TAG_DATA, MPI_COMM_WORLD, &mpi_mystat);
    MPI_Recv(buf_pravy.data(), pocet_prvku_pravy, MPI_CHAR, CHILD_RIGHT(myid), TAG_DATA, MPI_COMM_WORLD, &mpi_mystat);

    //merge
    vector<unsigned char> buf_vystup(pocet_prvku_celkem);
    merge(buf_levy.begin(), buf_levy.end(), buf_pravy.begin(), buf_pravy.end(), buf_vystup.begin());

    //odeslani rodici
    MPI_Send(&pocet_prvku_celkem, 1, MPI_INT, PARENT(myid), TAG_VELIKOST, MPI_COMM_WORLD);
    MPI_Send(buf_vystup.data(), pocet_prvku_celkem, MPI_CHAR, PARENT(myid), TAG_DATA, MPI_COMM_WORLD);
  }





  MPI_Finalize(); 
  return 0;
}

