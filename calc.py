#!/usr/bin/env python3

import math
import sys


pocet_prvku = int(str(sys.argv[1]))
print("pocet_prvku :", pocet_prvku)

#aby byl zarovnany binarni strom musi byt pocet listovych cepu zarovnany na 2^n
pocet_listovych_cpu = math.ceil( math.log2( pocet_prvku ) )
print("log2:", pocet_listovych_cpu)
n = 0
while(pocet_listovych_cpu > math.pow(2, n)):
    n+=1

print("n:", n)
pocet_listovych_cpu = math.pow(2, n)
print("pocet_listovych_cpu :", pocet_listovych_cpu)

pocet_prvku_na_cpu = math.ceil( pocet_prvku / pocet_listovych_cpu )
print("pocet_prvku_na_cpu :", pocet_prvku_na_cpu)

pocet_mist_na_listovych_cpu = pocet_prvku_na_cpu * pocet_listovych_cpu
print("pocet_mist_na_listovych_cpu :", pocet_mist_na_listovych_cpu)

pocet_mist_navic = pocet_mist_na_listovych_cpu - pocet_prvku
print("pocet_mist_navic :", pocet_mist_navic)

pocet_cpu_celkem = (pocet_listovych_cpu * 2) - 1
print("pocet_cpu_celkem :", pocet_cpu_celkem)



